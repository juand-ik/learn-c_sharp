using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class upTownFunction : MonoBehaviour {

	int health      = 100;
	int attackPower = 25;
	bool shieldOn   = true;
	int shieldAmt   = 15;
	
	void Start()
	{
		Debug.Log("Health at start " + health);
	}
	public void Attack()
	{
		int damageToInflict = GetAttackDamage(shieldOn, shieldAmt, attackPower);
		health -= damageToInflict;
		Debug.Log("Healt after attack: " + health);
	}
	public void Heal()
	{
		int healAmount = GetRandomNumber();
		health += healAmount;
		
		Debug.Log("Recived " + healAmount + "health");
		Debug.Log("You now have " + health + "health");
	}
	private int GetAttackDamage( bool isShieldOn, int theShieldAmt, int theAttackPower )
	{
		int damage = 0;
		
		if( isShieldOn )
		{
			damage = theAttackPower - (int)( (float)theShieldAmt * 0.10f );
		}
		else
		{
			damage = theAttackPower;
		}
		return damage;
	}
	private int GetRandomNumber()
	{
		return Random.Range(2,10);
	}
}