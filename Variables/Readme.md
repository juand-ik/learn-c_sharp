# Variables

C# Es un lenguaje fuertemente tipado.

Se declara el *tipo* + *nombredelavariable*.

```
string heroName = "SuperDev";
int hp = 100;
float shieldPower = 76.5;
double actualDamagePercent = .05;
```
