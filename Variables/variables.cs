using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class variables : MonoBehaviour
{
	// Use this for initialization
	void Start ()
	{
		string heroName = "SuperDev";
		string equippedWeapon = "Laptop";
		string favoriteFurniture = "Chair Gamer";

		Debug.Log( heroName );
		Debug.Log( equippedWeapon );
		Debug.Log( favoriteFurniture.ToUpper() );

		int hp = 100;
		float shieldPower = 76.5f;
		int laserDamage = 30;
		// double 2 digits
		double actualDamagePercent = .05;

		int actualDamage = (int)( laserDamage * actualDamagePercent );

		// Binary operation
		hp -= actualDamage; // hp = hp - actualDamage;

		shieldPower = shieldPower - ( laserDamage - actualDamage );
		// Concatenation
		Debug.Log( "HP: " + hp );
		Debug.Log( "Shield Power: " + shiledPower);

		int slices = 10/5;
		print( slices );

		int newDamage = 10/3;
		int newDamageRemainder = 10%3;

		print("10 divided by 3 is equals: " + newDamage + " with a remainder of " + newDamageRemainder);
	}

	// Update is called once per frame
	void Update () { }
}
