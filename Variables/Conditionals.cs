using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Conditionals : MonoBehaviour
{
	int playerOneTowersRemaining = 3;
	int playerTwoTowersRemaining = 3;

	bool playerOneMainTowersDestroyed = false;
	bool playerTwoMainTowersDestroyed = false;

	int timer = 200;

	// Use this for initialization
	void Start ()
	{
		if( playerOneMainTowersDestroyed || playerTwoMainTowersDestroyed )
		{
			if( playerOneMainTowersDestroyed )
			{
				Debug.Log("Player two wins");
			}
			else
			{
				Debug.Log("Player one wins");
			}
		}
		else if( timer <= 0 )
		{
			if( playerOneTowersRemaining < playerTwoTowersRemaining )
			{
				Debug.Log("Player two wins");
			}
			else if( playerTwoTowersRemaining < playerOneTowersRemaining )
			{
				Debug.Log("Player one wins");
			}
			else
			{
				Debug.Log("The game was a draw!!");
			}
		}

		if( true == false || false != true && 1 == 1)
		{
			Debug.Log("Did we get here");
		}
	}
	// Update is called once per frame
	void Update ()
	{
	}
}