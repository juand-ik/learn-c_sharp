using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Humanoid : MonoBehaviour
{
	protected int health;
	protected int attackDamage;
	protected float movementSpeed;
	
	public virtual void TakeDamage(int damageToTake)
	{
		health -= damageToTake;
	}
	public virtual int Attack()
	{
		return attackDamage;
	}
	public virtual void Move()
	{
		transform.transalete(Vector3.foward * Time.deltaTime);
	}
	public void Die()
	{
	}
}